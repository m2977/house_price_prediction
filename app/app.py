from flask import Flask, jsonify
from flask import request
from app.models import build_model

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/house-price', methods=['POST'])
def house_price():
    data = request.json['data']
    data = build_model.convert_data_type(data)
    final_data = build_model.pre_processing(data)
    model = build_model.load_model()
    price = model.predict(final_data).tolist()[0]
    return jsonify({"estimated_price": price})


if __name__ == '__main__':
    app.run()
