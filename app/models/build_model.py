import json
import pickle

import pandas as pd


def pre_processing(input_data: dict):
    # data = pd.read_csv(input_data)
    data = pd.DataFrame(data=input_data, index=[0])
    if 'T000000' in str(data.dayhours):
        data.dayhours = data.dayhours.str.replace('T000000', "")
    data.dayhours = pd.to_datetime(data.dayhours, format='%Y%m%d')
    data['month_year'] = data['dayhours'].apply(lambda x: x.strftime('%B-%Y'))
    data['has_basement'] = data['basement'].apply(lambda x: 'No' if x == 0 else 'Yes')
    data['has_renovated'] = data['yr_renovated'].apply(lambda x: 'No' if x == 0 else 'Yes')
    data['year'] = data['month_year'].apply(lambda x: int(x.split('-')[1]))
    data['age'] = data['year'] - data['yr_built']
    dummy = ['ceil', 'coast', 'sight', 'condition', 'quality', 'furnished', 'has_basement', 'has_renovated']
    # data_final = pd.get_dummies(data, columns=dummy, drop_first=True)
    # creating dummy variable
    for i in [1.5, 2, 2.5, 3, 3.5]:
        data['ceil_' + str(i)] = 0

    for i in range(1, 2):
        data['coast_' + str(i)] = 0
        data['furnished_' + str(i)] = 0

    for i in range(3, 14):
        data['quality_' + str(i)] = 0

    for i in range(1, 5):
        data['sight_' + str(i)] = 0

    for i in range(2, 6):
        data['condition_' + str(i)] = 0

    for i in ['Yes']:
        data['has_basement_' + i] = 0

    for i in ['Yes']:
        data['has_renovated_' + i] = 0

    for i in ['Yes']:
        if ((data['has_basement'] == i).bool()):
            data['has_basement_' + i] = 1

    for i in ['Yes']:
        if ((data['has_renovated'] == i).bool()):
            data['has_renovated_' + i] = 1

    for i in [1.5, 2.0, 2.5, 3.0, 3.5]:
        if ((data['ceil'] == i).bool()):
            data['ceil'] = 1

    for i in range(1, 2):
        if ((data['coast'] == i).bool()):
            data['coast_' + str(i)] = 1

    for i in range(1, 2):
        if ((data['furnished'] == i).bool()):
            data['furnished_' + str(i)] = 1

    for i in range(3, 14):
        if ((data['quality'] == i).bool()):
            data['quality_' + str(i)] = 1

    for i in range(1, 5):
        if ((data['sight'] == i).bool()):
            data['sight_' + str(i)] = 1

    for i in range(2, 6):
        if ((data['condition'] == i).bool()):
            data['condition_' + str(i)] = 1

    drop_cols = ['cid', 'dayhours', 'yr_built', 'year', 'zipcode', 'basement', 'month_year', 'yr_renovated', 'price',
                 'ceil', 'coast', 'sight', 'condition', 'quality', 'furnished', 'has_basement', 'has_renovated']
    data_final = data.drop(drop_cols, axis=1)
    return data_final


def load_model():
    with open('D:\Data\GreatLearning\Capstone\house_price_prediction\data\model_pickle', 'rb') as f:
        mp = pickle.load(f)
        return mp


# X_test = model("../../data/innercity.csv")
sample = {'cid': 3034200666, 'dayhours': '20141107T000000', 'price': 808100, 'room_bed': 4, 'room_bath': 3.25,
          'living_measure': 3020, 'lot_measure': 13457, 'ceil': 1, 'coast': 0, 'sight': 0, 'condition': 5, 'quality': 9,
          'ceil_measure': 3020, 'basement': 0, 'yr_built': 1956, 'yr_renovated': 0, 'zipcode': 98133, 'lat': 47.7174,
          'long': -122.336, 'living_measure15': 2120, 'lot_measure15': 7553, 'furnished': 1, 'total_area': 16477}


def convert_data_type(data):
    with open('D:\Data\GreatLearning\Capstone\house_price_prediction\data\data_type.json', 'r') as rp:
        dtypes: dict = json.load(rp)
        for key, value in data.items():
            if dtypes[key] == 'int64':
                data[key] = int(value)
            elif dtypes[key] == 'float64':
                data[key] = float(value)
    return data

# convert_data_type(sample)
